import mongoose from "mongoose";
import * as dotenv from "dotenv";
dotenv.config();
 
const { ATLAS_URI } = process.env;
 
if (!ATLAS_URI) {
   console.error("No ATLAS_URI environment variable has been defined in config.env");
   process.exit(1);
}
const db_server  = process.env.DB_ENV || 'primary';

export async function connect() {
    try {
        mongoose.set('strictQuery', false);
        mongoose.connection.on("connected", function(ref) {
                    console.log("Connected to " + db_server + " DB!");
                })

        
                // If the connection throws an error
                mongoose.connection.on("error", function(err) {
                    console.error('Failed to connect to DB ' + db_server + ' on startup ', err);
  });
  
  // When the connection is disconnected
  mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection to DB :' + db_server + ' disconnected');
  });
  
  const gracefulExit = function() { 
      mongoose.connection.close(function () {
      console.log('Mongoose default connection with DB :' + db_server + ' is disconnected through app termination');
      process.exit(0);
    });
  }
  
  // If the Node process ends, close the Mongoose connection
  process.on('SIGINT', gracefulExit).on('SIGTERM', gracefulExit);
  
  
  // options.server.socketOptions = options.replset.socketOptions = { keepAlive: 1 };
  // mongoose.connect(process.env.ATLAS_URI);
  // console.log("Trying to connect to DB " + db_server);
  mongoose.connect(process.env.ATLAS_URI)
  
  
  
} catch (error) {
    const ERROR = new Error()
    ERROR.message = "CAN'T CONNECT DB!!! => "+error.message
    ERROR.name = "MongoError"
    ERROR.stack = __dirname+"database"
    throw ERROR
}
}