import { ObjectID } from "bson";


export interface User {
    name:string;
    mail:string;
    password:string;
    _id?: ObjectID
}