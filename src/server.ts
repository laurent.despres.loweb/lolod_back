import * as dotenv from "dotenv";
import cors from "cors";
import express from "express";
import { connectToDatabase } from "./database";
import { baseRouter } from "./routes";
import { nextTick } from "process";
import {connect} from "./config/database"
const app = express();

// Load environment variables from the .env file, where the ATLAS_URI is configured
dotenv.config();
 
const { ATLAS_URI } = process.env;
 
if (!ATLAS_URI) {
   console.error("No ATLAS_URI environment variable has been defined in config.env");
   process.exit(1);
}
app.use(cors());
connect()
app.use("/back",baseRouter);
 
// connectToDatabase(ATLAS_URI)
//    .then(() => {
 
//        // start the Express server
//        app.listen(2222, () => {
//            console.log(`Server running at http://localhost:2222...`);
//        });
//        app.use("/back", (req,res, next) => {console.log(req.url); next()})
 
//    })
//    .catch(error => console.error(error));