import * as express from "express";
import { userRouter } from "./user.routes";


export const baseRouter = express.Router();

baseRouter.use(express.json())
    .use("/user", userRouter);