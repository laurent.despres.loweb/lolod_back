import {Schema, Types} from "mongoose";
import { User } from "../interfaces/User";

const ObjectId = Types.ObjectId;

const userSchema = new Schema<User>({
    name: { type: String, required: true },
    mail: { type: String, required: true },
    password: { type: String,required:true }
  });